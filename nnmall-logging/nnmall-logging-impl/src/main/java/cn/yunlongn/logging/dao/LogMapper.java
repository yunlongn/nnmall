package cn.yunlongn.logging.dao;

import cn.yunlongn.logging.dataobject.LogDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface LogMapper extends BaseMapper<LogDO> {


}
