package cn.yunlongn.logging.dataobject;

import cn.yunlongn.mall.common.core.dataobject.BaseDO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

/**
 * @author yunlongn
 * @date 2019-09-10
 */
@Data
@Accessors(chain = true)
public class LogDO extends BaseDO {


    private Long id;

    /**
     * 操作用户
     */
    private String username;

    /**
     * 描述
     */
    private String description;

    /**
     * 方法名
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 请求ip
     */
    private String requestIp;

    private String address;

    private String serverName;

    /**
     * 请求耗时
     */
    private Long time;

    /**
     * 异常详细
     */
    private byte[] exceptionDetail;

    public LogDO(String logType, Long time) {
        this.logType = logType;
        this.time = time;
    }
}
