package cn.yunlongn.logging.api.dto;

import lombok.Data;

/**
 * 日志查询类
 * @author Zheng Jie
 * @date 2019-6-4 09:23:07
 */
@Data
public class LogQueryCriteria {

    // 多字段模糊
    private String blurry;

    private String logType;
}
