package cn.yunlongn.logging.api.bo;

public class LogBO {

    private Long id;

    /**
     * 操作用户
     */
    private String username;

    /**
     * 描述
     */
    private String description;

    /**
     * 方法名
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 请求ip
     */
    private String requestIp;

    private String address;

    private String serverName;

    /**
     * 请求耗时
     */
    private Long time;

}
