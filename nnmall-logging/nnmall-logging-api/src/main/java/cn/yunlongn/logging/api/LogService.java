package cn.yunlongn.logging.api;

import cn.yunlongn.logging.api.bo.LogBO;
import cn.yunlongn.logging.api.dto.LogQueryCriteria;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import cn.yunlongn.mall.common.core.vo.PageParam;
import cn.yunlongn.mall.common.core.vo.PageResult;

public interface LogService {

    /**
     * queryAll
     * @param criteria
     * @param pageable
     * @return
     */
    CommonResult<LogBO> queryAll(LogQueryCriteria criteria, PageParam pageable);

}
