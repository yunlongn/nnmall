package cn.yunlongn.mall.admin.convert;

import cn.yunlongn.mall.admin.api.bo.resource.ResourceBO;
import cn.yunlongn.mall.admin.api.dto.resource.ResourceAddDTO;
import cn.yunlongn.mall.admin.api.dto.resource.ResourceUpdateDTO;
import cn.yunlongn.mall.admin.dataobject.ResourceDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

@Mapper
public interface ResourceConvert {

    ResourceConvert INSTANCE = Mappers.getMapper(ResourceConvert.class);

    @Mappings({
            @Mapping(source = "permissions", target = "permissions", qualifiedByName = "translateStringFromList")
    })
    ResourceDO convert(ResourceAddDTO resourceAddDTO);

    @Mappings({
            @Mapping(source = "permissions", target = "permissions", qualifiedByName = "translateStringFromList")
    })
    ResourceDO convert(ResourceUpdateDTO resourceUpdateDTO);

    @Mappings({
            @Mapping(source = "permissions", target = "permissions", qualifiedByName = "translateListFromString")
    })
    ResourceBO convert(ResourceDO resourceDO);


    @Mappings({})
    List<ResourceBO> convert(List<ResourceDO> resourceDOs);


    @Named("translateListFromString")
    default List<String> translateListFromString(String str) {
        String[] stringArray = StringUtils.tokenizeToStringArray(str, ",");
        return Arrays.asList(stringArray);
    }

    @Named("translateStringFromList")
    default String translateStringFromList(List<String> list) {
        return StringUtils.collectionToDelimitedString(list, ",");
    }
}
