package cn.yunlongn.mall.admin.dao;

import cn.yunlongn.mall.admin.dataobject.OAuth2RefreshTokenDO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface OAuth2RefreshTokenMapper extends BaseMapper<OAuth2RefreshTokenDO> {

    default int updateToInvalid(Integer userId, Integer userType) {
        LambdaQueryWrapper<OAuth2RefreshTokenDO> query = new QueryWrapper<OAuth2RefreshTokenDO>()
                .lambda().eq(OAuth2RefreshTokenDO::getUserId, userId).eq(OAuth2RefreshTokenDO::getUserType, userType)
                .eq(OAuth2RefreshTokenDO::getValid, true);
        return update(new OAuth2RefreshTokenDO().setValid(false), query);
    }

}
