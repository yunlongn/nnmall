package cn.yunlongn.mall.admin.service;

import cn.yunlongn.mall.admin.api.RoleService;
import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.constant.AdminErrorCodeEnum;
import cn.yunlongn.mall.admin.api.dto.role.RoleAddDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleAssignResourceDTO;
import cn.yunlongn.mall.admin.api.dto.role.RolePageDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleUpdateDTO;
import cn.yunlongn.mall.admin.convert.RoleConvert;
import cn.yunlongn.mall.admin.dao.AdminRoleMapper;
import cn.yunlongn.mall.admin.dao.RoleMapper;
import cn.yunlongn.mall.admin.dao.RoleResourceMapper;
import cn.yunlongn.mall.admin.dataobject.ResourceDO;
import cn.yunlongn.mall.admin.dataobject.RoleDO;
import cn.yunlongn.mall.admin.dataobject.RoleResourceDO;
import cn.yunlongn.mall.common.core.constant.DeletedStatusEnum;
import cn.yunlongn.mall.common.core.util.CollectionUtil;
import cn.yunlongn.mall.common.core.util.ServiceExceptionUtil;
import cn.yunlongn.mall.common.core.vo.PageResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RestController
@RequestMapping("/feign")
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleMapper roleMapper;
    @Autowired
    AdminRoleMapper adminRoleMapper;
    @Autowired
    RoleResourceMapper roleResourceMapper;

    @Autowired
    private ResourceServiceImpl resourceService;
    @Override
    public PageResult<RoleBO> getRolePage(RolePageDTO rolePageDTO) {
        IPage<RoleDO> page = roleMapper.selectPage(rolePageDTO);
        return RoleConvert.INSTANCE.convert(page);
    }

    @Override
    public List<RoleBO> getRoleList() {
        List<RoleDO> roles = roleMapper.selectList();
        return RoleConvert.INSTANCE.convert(roles);
    }

    @Override
    public List<RoleBO> getRoleList(Collection<Integer> ids) {
        List<RoleDO> roles = roleMapper.selectBatchIds(ids);
        return RoleConvert.INSTANCE.convert(roles);
    }

    @Override
    public RoleBO addRole(Integer adminId, RoleAddDTO roleAddDTO) {
        // TODO 角色名是否要唯一呢？貌似一般系统都是允许的。
        // 保存到数据库
        RoleDO role = RoleConvert.INSTANCE.convert(roleAddDTO);
        role.setCreateTime(new Date());
        role.setDeleted(DeletedStatusEnum.DELETED_NO.getValue());
        roleMapper.insert(role);
        // TODO 插入操作日志
        // 返回成功
        return RoleConvert.INSTANCE.convert(role);
    }

    @Override
    public Boolean updateRole(Integer adminId, RoleUpdateDTO roleUpdateDTO) {

        // 校验角色是否存在
        if (roleMapper.selectById(roleUpdateDTO.getId()) == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.RESOURCE_NOT_EXISTS.getCode());
        }
        RoleDO role = RoleConvert.INSTANCE.convert(roleUpdateDTO);
        roleMapper.updateById(role);
        return true;
    }

    @Override
    public Boolean deleteRole(Integer adminId, Integer roleId) {
        // 校验角色是否存在
        if (roleMapper.selectById(roleId) == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.RESOURCE_NOT_EXISTS.getCode());
        }
        // 更新到数据库，标记删除
        roleMapper.deleteById(roleId);
        // 标记删除 RoleResource
        roleResourceMapper.deleteByRoleId(roleId);
        // 标记删除 AdminRole
        // 标记删除用户关联的用户
        adminRoleMapper.deleteByRoleId(roleId);
        // TODO 插入操作日志
        // 返回成功
        return true;
    }

    @Override
    @Transactional
    public Boolean assignRoleResource(Integer adminId, RoleAssignResourceDTO roleAssignResourceDTO) {
        Integer roleId = roleAssignResourceDTO.getId();
        Set<Integer> resourceIds = roleAssignResourceDTO.getResourceIds();
        // 校验角色是否存在
        if (roleMapper.selectById(roleAssignResourceDTO.getId()) == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ROLE_NOT_EXISTS.getCode());
        }

        // 校验是否有不存在的资源
        if (!CollectionUtil.isEmpty(resourceIds)) {
            // 传入资源列表id 查询资源是否存在返回查出来的总数 与传入的总数是否能对上
            Integer resourcesSize = resourceService.getResources(resourceIds);
            if (resourcesSize != resourceIds.size()) {
                throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ROLE_ASSIGN_RESOURCE_NOT_EXISTS.getCode());
            }
        }

        // TODO 这里先简单实现。即方式是，删除老的分配的资源关系，然后添加新的分配的资源关系
        // 标记角色原资源关系都为删除
        roleResourceMapper.deleteByRoleId(roleId);
        // 创建 RoleResourceDO 数组，并插入到数据库
        if (!CollectionUtil.isEmpty(resourceIds)) {
            List<RoleResourceDO> roleResources = resourceIds.stream().map(resourceId -> {
                // 将每一个资源对象对应roleId
                RoleResourceDO roleResource = new RoleResourceDO().setRoleId(roleId).setResourceId(resourceId);
                roleResource.setCreateTime(new Date());
                roleResource.setDeleted(DeletedStatusEnum.DELETED_NO.getValue());
                return roleResource;
            }).collect(Collectors.toList());
            // 插入这一组资源

            roleResourceMapper.insertList(roleResources);
        }
        // TODO 插入操作日志
        // 返回成功
        return true;
    }


    /**
     * 用角色id 换取角色实体列表
     * @param roleIds
     * @return
     */
    public List<RoleDO> getRoles(Set<Integer> roleIds) {
        if (CollectionUtil.isEmpty(roleIds)) {
            return Collections.emptyList();
        }
        return roleMapper.selectBatchIds(roleIds);
    }



    /**
     * 获得权限与角色的映射关系。
     *
     * TODO ，等以后有 redis ，优化成从缓存读取。每个 permission ，哪些角色可以访问
     * TODO 将 permission -> role 放在redis中 读取直接从redis中读取
     *
     * @param permissions 权限标识数组
     * @return 映射关系。KEY：权限标识；VALUE：角色编号数组
     */
    public Map<String, List<Integer>> getPermissionRoleMap(List<String> permissions) {
        if (CollectionUtil.isEmpty(permissions)) {
            return Collections.emptyMap();
        }
        Map<String, List<Integer>> result = Maps.newHashMapWithExpectedSize(permissions.size());
        for (String permission : permissions) {
            List<ResourceDO> resources = resourceService.getResourceListByPermission(permission);
            if (resources.isEmpty()) { // 无需授权
                result.put(permission, Collections.emptyList());
            } else {
                List<RoleResourceDO> roleResources = roleResourceMapper.selectListByResourceId(
                        CollectionUtil.convertSet(resources, ResourceDO::getId));
//                roleResources.stream().map()
                if (roleResources.isEmpty()) {
                    result.put(permission, Collections.emptyList());
                } else {
                    result.put(permission, CollectionUtil.convertList(roleResources, RoleResourceDO::getRoleId));
                }
            }
        }
        return result;
    }
}
