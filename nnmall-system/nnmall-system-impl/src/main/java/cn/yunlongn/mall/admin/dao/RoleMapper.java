package cn.yunlongn.mall.admin.dao;

import cn.yunlongn.mall.admin.api.dto.role.RolePageDTO;
import cn.yunlongn.mall.admin.dataobject.RoleDO;
import cn.yunlongn.mall.common.core.mybatis.QueryWrapperX;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper extends BaseMapper<RoleDO> {

    default List<RoleDO> selectList() {
        return selectList(new QueryWrapper<>());
    }

    default IPage<RoleDO> selectPage(RolePageDTO rolePageDTO) {
        return selectPage(new Page<>(rolePageDTO.getPageNo(), rolePageDTO.getPageSize()),
                new QueryWrapperX<RoleDO>().likeIfPresent("name", rolePageDTO.getName()));
    }

}
