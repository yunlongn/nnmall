package cn.yunlongn.mall.admin.dao;

import cn.yunlongn.mall.admin.api.dto.admin.AdminPageDTO;
import cn.yunlongn.mall.common.core.mybatis.QueryWrapperX;
import cn.yunlongn.mall.admin.dataobject.AdminDO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper extends BaseMapper<AdminDO> {

    default AdminDO selectByUsername(@Param("username") String username) {
        return selectOne(new QueryWrapper<AdminDO>().lambda().eq(AdminDO::getUsername, username));
    }

    default IPage<AdminDO> selectPage(AdminPageDTO adminPageDTO) {
        return selectPage(new Page<>(adminPageDTO.getPageNo(), adminPageDTO.getPageSize()),
                new QueryWrapperX<AdminDO>().likeIfPresent("nickname", adminPageDTO.getNickname()));
    }



}
