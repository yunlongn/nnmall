package cn.yunlongn.mall.admin.dao;

import cn.yunlongn.mall.admin.dataobject.OAuth2AccessTokenDO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface OAuth2AccessTokenMapper extends BaseMapper<OAuth2AccessTokenDO> {

    default int updateToInvalid(Integer userId, Integer userType) {
        // 将用户的状态改成传入的状态
        LambdaQueryWrapper<OAuth2AccessTokenDO> query = new QueryWrapper<OAuth2AccessTokenDO>()
                .lambda().eq(OAuth2AccessTokenDO::getUserId, userId).eq(OAuth2AccessTokenDO::getUserType, userType)
                .eq(OAuth2AccessTokenDO::getValid, true);
        return update(new OAuth2AccessTokenDO().setValid(false), query);
    }

    default int updateToInvalidByRefreshToken(String refreshToken) {
        LambdaQueryWrapper<OAuth2AccessTokenDO> query = new QueryWrapper<OAuth2AccessTokenDO>()
                .lambda().eq(OAuth2AccessTokenDO::getRefreshToken, refreshToken).eq(OAuth2AccessTokenDO::getValid, true);
        return update(new OAuth2AccessTokenDO().setValid(false), query);
    }

}
