package cn.yunlongn.mall.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/***
 * 添加swagger的扫描  ，  全局拦截的扫描
 * @author Yun
 */
@SpringBootApplication(scanBasePackages = {
        "cn.yunlongn.mall.admin",
        "cn.yunlongn.mall.spring.core.swagger",
        "cn.yunlongn.mall.spring.core.web.handler"})
@EnableAsync(proxyTargetClass = true)
@EnableEurekaClient
@EnableFeignClients(basePackages = {"cn.yunlongn.mall.admin"})
public class SystemServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemServerApplication.class, args);
    }

}
