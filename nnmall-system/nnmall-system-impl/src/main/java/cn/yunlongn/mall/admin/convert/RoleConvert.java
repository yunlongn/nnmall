package cn.yunlongn.mall.admin.convert;


import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.dto.role.RoleAddDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleUpdateDTO;
import cn.yunlongn.mall.admin.dataobject.RoleDO;
import cn.yunlongn.mall.common.core.vo.PageResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoleConvert {

    RoleConvert INSTANCE = Mappers.getMapper(RoleConvert.class);

    @Mappings({
            @Mapping(source = "records", target = "list")
    })
    PageResult<RoleBO> convert(IPage<RoleDO> roles);

    @Mappings({})
    List<RoleBO> convert(List<RoleDO> roles);

    @Mappings({})
    RoleDO convert(RoleAddDTO roles);

    @Mappings({})
    RoleDO convert(RoleUpdateDTO roles);

    @Mappings({})
    RoleBO convert(RoleDO roles);
}
