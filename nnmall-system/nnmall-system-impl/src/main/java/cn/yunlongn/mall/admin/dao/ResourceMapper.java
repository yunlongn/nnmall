package cn.yunlongn.mall.admin.dao;


import cn.yunlongn.mall.admin.dataobject.ResourceDO;
import cn.yunlongn.mall.common.core.mybatis.QueryWrapperX;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ResourceMapper extends BaseMapper<ResourceDO> {

    // TODO yunlongn，后续改造。
//    List<ResourceDO> selectListByTypeAndRoleIds(@Param("type") Integer type,
//                                                @Param("roleIds") Set<Integer> roleIds);


    default List<ResourceDO> selectListByTypeAndRoleIds(@Param("type") Integer type,
                                                        @Param("roleIds") Set<Integer> roleIds) {
        return selectList(new QueryWrapperX<ResourceDO>().eqIfPresent("type", type).in("role_id",roleIds));
//        return selectList(new QueryWrapperX<ResourceDO>().like("permissions", permission));
    }



    /***
     * 使用like的时候要小心
     * 如果传过来的为空的话就不要把这个字段加入查询字段里面了，浪费性能
     * @param permission
     * @return
     */
    default List<ResourceDO> selectListByPermission(String permission) {
        return selectList(new QueryWrapperX<ResourceDO>().like("permissions", permission));
    }

    default List<ResourceDO> selectListByType(Integer type) {
        return selectList(new QueryWrapperX<ResourceDO>().eqIfPresent("type", type));
    }

    default Integer selectListByIds(Set<Integer> ids) {
        return selectCount(new QueryWrapper<ResourceDO>().lambda().in(ResourceDO::getId, ids));
    }

    default int selectCountByPid(Integer pid) {
        return selectCount(new QueryWrapper<ResourceDO>().lambda().eq(ResourceDO::getPid, pid));
    }

}
