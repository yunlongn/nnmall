package cn.yunlongn.mall.admin.service;


import cn.yunlongn.mall.admin.api.AdminService;
import cn.yunlongn.mall.admin.api.OAuth2Service;
import cn.yunlongn.mall.admin.api.bo.admin.AdminAuthenticationBO;
import cn.yunlongn.mall.admin.api.bo.admin.AdminAuthorizationBO;
import cn.yunlongn.mall.admin.api.bo.admin.AdminBO;
import cn.yunlongn.mall.admin.api.bo.oauth2.OAuth2AccessTokenBO;
import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.constant.AdminConstants;
import cn.yunlongn.mall.admin.api.constant.AdminErrorCodeEnum;
import cn.yunlongn.mall.admin.api.dto.admin.*;
import cn.yunlongn.mall.admin.api.dto.oauth2.OAuth2CreateTokenDTO;
import cn.yunlongn.mall.admin.api.dto.oauth2.OAuth2RemoveTokenByUserDTO;
import cn.yunlongn.mall.admin.convert.AdminConvert;
import cn.yunlongn.mall.admin.dao.AdminMapper;
import cn.yunlongn.mall.admin.dao.AdminRoleMapper;
import cn.yunlongn.mall.admin.dataobject.AdminDO;
import cn.yunlongn.mall.admin.dataobject.AdminRoleDO;
import cn.yunlongn.mall.admin.dataobject.RoleDO;
import cn.yunlongn.mall.common.core.constant.CommonStatusEnum;
import cn.yunlongn.mall.common.core.constant.DeletedStatusEnum;
import cn.yunlongn.mall.common.core.constant.UserTypeEnum;
import cn.yunlongn.mall.common.core.util.CollectionUtil;
import cn.yunlongn.mall.common.core.util.ServiceExceptionUtil;
import cn.yunlongn.mall.common.core.vo.PageResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;


@Service
@RestController
@RequestMapping("/feign")
public class AdminServiceImpl extends ServiceImpl<AdminMapper, AdminDO> implements AdminService, IService<AdminDO> {

    @Autowired
    AdminMapper adminMapper;
    @Autowired
    OAuth2Service oauth2Service;
    @Autowired
    RoleServiceImpl roleService;
    @Autowired
    AdminRoleMapper adminRoleMapper;

    private String encodePassword(String password) {
        return DigestUtils.md5DigestAsHex(password.getBytes());
    }

    @Override
    public AdminAuthenticationBO authentication(@Validated AdminAuthenticationDTO adminAuthenticationDTO) {
        // 1、 判断账号是否存在
        AdminDO adminDO = adminMapper.selectByUsername(adminAuthenticationDTO.getUsername());
        // 当账号不存在的时候
        if (adminDO == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_NOT_REGISTERED.getCode());
        }
        // 传递过来的密码不正确

        if (!encodePassword(adminAuthenticationDTO.getPassword()).equals(adminDO.getPassword())) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_PASSWORD_ERROR.getCode());
        }

        // 判断账号状态被禁用的时候
        if (CommonStatusEnum.DISABLE.getValue().equals(adminDO.getStatus())) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_IS_DISABLE.getCode());
        }
        // 创建 accessToken
        OAuth2AccessTokenBO accessTokenBO = oauth2Service.createToken(new OAuth2CreateTokenDTO().setUserId(adminDO.getId())
                .setUserType(UserTypeEnum.ADMIN.getValue()));
        // 将登录的用户信息和token信息返回
        return AdminConvert.INSTANCE.convert2(adminDO).setToken(accessTokenBO);
    }


    @Override
    public PageResult<AdminBO> getAdminPage(AdminPageDTO adminPageDTO){

        IPage<AdminDO> page = adminMapper.selectPage(adminPageDTO);

        return AdminConvert.INSTANCE.convert(page);
    }

    @Override
    @Transactional
    public AdminBO addAdmin(Integer adminId, AdminAddDTO adminAddDTO) {
        // 校验账号唯一 用户名存在则甩出异常 用户存在
        if (!ObjectUtils.isEmpty(adminMapper.selectByUsername(adminAddDTO.getUsername()))) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_EXISTS.getCode());
        }
        // 保存到数据库
        AdminDO admin = AdminConvert.INSTANCE.convert(adminAddDTO)
                .setPassword(encodePassword(adminAddDTO.getPassword())) // 加密密码
                .setStatus(CommonStatusEnum.ENABLE.getValue());
        admin.setCreateTime(new Date());
        admin.setDeleted(DeletedStatusEnum.DELETED_NO.getValue());

        adminMapper.insert(admin);
        // TODO 插入操作日志
        // 返回成功
        return AdminConvert.INSTANCE.convert(admin);
    }


    @Override
    @Transactional
    public Boolean updateAdmin(Integer adminId, AdminUpdateDTO adminUpdateDTO) {
        // 校验账号存在
        AdminDO admin = adminMapper.selectById(adminUpdateDTO.getId());
        if (admin == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_NOT_REGISTERED.getCode());
        }
        if (AdminConstants.USERNAME_ADMIN.equals(admin.getUsername())
                || AdminConstants.USERNAME_DEMO.equals(admin.getUsername())) { // 特殊账号，不允许编辑
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_ADMIN_CAN_NOT_UPDATE.getCode());
        }
        // 校验账号唯一
        AdminDO usernameAdmin = adminMapper.selectByUsername(adminUpdateDTO.getUsername());
        if (usernameAdmin != null && !usernameAdmin.getId().equals(adminUpdateDTO.getId())) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_EXISTS.getCode());
        }
        if (StringUtils.isEmpty(adminUpdateDTO.getPassword())) {
            adminUpdateDTO.setPassword(encodePassword(adminUpdateDTO.getPassword()));
        }

        // 更新到数据库
        AdminDO updateAdmin = AdminConvert.INSTANCE.convert(adminUpdateDTO);
        adminMapper.updateById(updateAdmin);
        // TODO 插入操作日志
        // 返回成功
        return true;
    }



    @Override
    @Transactional
    public Boolean updateAdminStatus(Integer adminId, AdminUpdateStatusDTO adminUpdateStatusDTO) {
        //修改用户状态  用户标记删除
        System.out.println(adminUpdateStatusDTO.getId());
        // 校验账号存在
        AdminDO admin = adminMapper.selectById(adminUpdateStatusDTO.getId());
        if (admin == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_NOT_REGISTERED.getCode());
        }
        if (AdminConstants.USERNAME_ADMIN.equals(admin.getUsername())
                || AdminConstants.USERNAME_DEMO.equals(admin.getUsername())) { // 特殊账号，不允许编辑
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_ADMIN_STATUS_CAN_NOT_UPDATE.getCode());
        }
        // 如果状态相同，则返回错误 更改状态和之前的状态相同 则返回提示信息
        if (adminUpdateStatusDTO.getStatus().equals(admin.getStatus())) {
//            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_STATUS_EQUALS.getCode());
            return true;
        }
        // 更新管理员状态
        AdminDO updateAdmin = new AdminDO().setId(adminUpdateStatusDTO.getId()).setStatus(adminUpdateStatusDTO.getStatus());
        adminMapper.updateById(updateAdmin);
        // 如果是关闭当前用户的状态
        // 如果是关闭管理员，则标记 token 失效。 否则，管理员还可以继续蹦跶
        if(adminId.equals(admin.getId())){
            if (CommonStatusEnum.DISABLE.getValue().equals(adminUpdateStatusDTO.getStatus())) {
                oauth2Service.removeToken(new OAuth2RemoveTokenByUserDTO().setUserId(adminId).setUserType(UserTypeEnum.ADMIN.getValue()));
            }
        }


        // TODO 插入操作日志
        // 返回成功
        return true;
    }



    @Override
    @Transactional
    public Boolean deleteAdmin(Integer adminId, Integer updateAdminId) {
        // 校验账号存在
        AdminDO admin = adminMapper.selectById(updateAdminId);
        if (admin == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_NOT_REGISTERED.getCode());
        }
        // 只有禁用的账号才可以删除
        if (CommonStatusEnum.ENABLE.getValue().equals(admin.getStatus())) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_DELETE_ONLY_DISABLE.getCode());
        }
        // 标记删除 AdminDO
        adminMapper.deleteById(updateAdminId); // 标记删除
        // 标记删除 AdminRole
        adminRoleMapper.deleteByAdminId(updateAdminId);
        // TODO 插入操作日志
        // 返回成功
        return true;
    }


    @Override
    @Transactional
    public Boolean assignAdminRole(Integer adminId, AdminAssignRoleDTO adminAssignRoleDTO) {
        // 校验账号存在
        AdminDO admin = adminMapper.selectById(adminAssignRoleDTO.getId());
        if (admin == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_NOT_REGISTERED.getCode());
        }
        // 校验是否有不存在的角色
        if (!CollectionUtil.isEmpty(adminAssignRoleDTO.getRoleIds())) {
            List<RoleDO> roles = roleService.getRoles(adminAssignRoleDTO.getRoleIds());
            if (roles.size() != adminAssignRoleDTO.getRoleIds().size()) {
                throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_ASSIGN_ROLE_NOT_EXISTS.getCode());
            }
        }
        // TODO 这里先简单实现。即方式是，删除老的分配的角色关系，然后添加新的分配的角色关系
        // 标记管理员角色源关系都为删除
        adminRoleMapper.deleteByAdminId(adminAssignRoleDTO.getId());
        // 创建 RoleResourceDO 数组，并插入到数据库
        if (!CollectionUtil.isEmpty(adminAssignRoleDTO.getRoleIds())) {
            List<AdminRoleDO> adminRoleDOs = adminAssignRoleDTO.getRoleIds().stream().map(roleId -> {
                AdminRoleDO roleResource = new AdminRoleDO().setAdminId(adminAssignRoleDTO.getId()).setRoleId(roleId);
                roleResource.setCreateTime(new Date());
                roleResource.setDeleted(DeletedStatusEnum.DELETED_NO.getValue());
                return roleResource;
            }).collect(Collectors.toList());
//            roleService.saveBatch(adminRoleDOs);
            adminRoleMapper.insertList(adminRoleDOs);
        }
        // TODO 插入操作日志
        // 返回成功
        return true;
    }
    @Override
    public Map<Integer, Collection<RoleBO>> getAdminRolesMap(@Validated @RequestBody Collection<Integer> adminIds) {

        // 查询管理员拥有的角色关联数据
        List<AdminRoleDO> adminRoleList = adminRoleMapper.selectListByAdminIds(adminIds);
        if (adminRoleList.isEmpty()) {
            return Collections.emptyMap();
        }
        // 查询角色数据
        List<RoleBO> roleList = roleService.getRoleList(CollectionUtil.convertSet(adminRoleList, AdminRoleDO::getRoleId));
        Map<Integer, RoleBO> roleMap = CollectionUtil.convertMap(roleList, RoleBO::getId);
        // 拼接数据
        Multimap<Integer, RoleBO> result = ArrayListMultimap.create();
        adminRoleList.forEach(adminRole -> result.put(adminRole.getAdminId(), roleMap.get(adminRole.getRoleId())));

        return result.asMap();
    }

    @Override
    public List<RoleBO> getRoleList(@RequestParam Integer adminId) {
        // 校验账号存在
        AdminDO admin = adminMapper.selectById(adminId);
        if (admin == null) {
            throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_USERNAME_NOT_REGISTERED.getCode());
        }
        // 查询管理员拥有的角色关联数据 查询 admin - admin_role
        List<AdminRoleDO> adminRoleList = adminRoleMapper.selectByAdminId(adminId);
        if (adminRoleList.isEmpty()) {
            // 如果为空返回空的数组
            return Collections.emptyList();
        }
        // 查询角色数据
        return roleService.getRoleList(CollectionUtil.convertSet(adminRoleList, AdminRoleDO::getRoleId));
    }

    @Override
    public AdminAuthorizationBO checkPermissions(Integer adminId, List<String> permissions) {
        // 查询管理员拥有的角色关联数据
        List<AdminRoleDO> adminRoleList = adminRoleMapper.selectByAdminId(adminId);
//        adminRoleList.stream().
        Set<Integer> adminRoleIds = CollectionUtil.convertSet(adminRoleList, AdminRoleDO::getRoleId);
        // 授权校验
        if (!CollectionUtil.isEmpty(permissions)) {
            Map<String, List<Integer>> permissionRoleMap = roleService.getPermissionRoleMap(permissions);

            for (Map.Entry<String, List<Integer>> entry : permissionRoleMap.entrySet()) {
                if (!CollectionUtil.containsAny(entry.getValue(), adminRoleIds)) { // 当里entry都不满足的情况下 报错没有权限
                    throw ServiceExceptionUtil.exception(AdminErrorCodeEnum.ADMIN_INVALID_PERMISSION.getCode());
                }
            }
        }
        // 获得用户
        AdminDO admin = adminMapper.selectById(adminId);
        // 返回成功
        return new AdminAuthorizationBO().setId(adminId).setUsername(admin.getUsername())
                .setRoleIds(adminRoleIds);
    }

}
