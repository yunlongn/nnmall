package cn.yunlongn.mall.admin.convert;



import cn.yunlongn.mall.admin.api.bo.datadict.DataDictBO;
import cn.yunlongn.mall.admin.api.dto.datadict.DataDictAddDTO;
import cn.yunlongn.mall.admin.api.dto.datadict.DataDictUpdateDTO;
import cn.yunlongn.mall.admin.dataobject.DataDictDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DataDictConvert {

    DataDictConvert INSTANCE = Mappers.getMapper(DataDictConvert.class);


    @Mappings({})
    DataDictDO convert(DataDictAddDTO dataDictAddDTO);

    @Mappings({})
    DataDictDO convert(DataDictUpdateDTO dataDictAddDTO);

    @Mappings({})
    DataDictBO convert(DataDictDO dictDO);

    @Mappings({})
    List<DataDictBO> convert(List<DataDictDO> dictDO);


}
