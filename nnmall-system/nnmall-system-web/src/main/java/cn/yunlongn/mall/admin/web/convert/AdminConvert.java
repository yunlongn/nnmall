package cn.yunlongn.mall.admin.web.convert;

import cn.yunlongn.mall.admin.api.bo.admin.AdminBO;
import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.sdk.context.AdminSecurityContext;
import cn.yunlongn.mall.admin.web.vo.admin.AdminRoleVO;
import cn.yunlongn.mall.admin.web.vo.admin.AdminVO;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import cn.yunlongn.mall.common.core.vo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.Collection;
import java.util.List;

@Mapper
public interface AdminConvert {

    AdminConvert INSTANCE = Mappers.getMapper(AdminConvert.class);

//    @Mappings({})
//    AdminInfoVO convert(AdminSecurityContext adminSecurityContext);

    @Mappings({})
    AdminVO convert(AdminBO adminBO);

    @Mappings({})
    CommonResult<AdminVO> convert2(CommonResult<AdminBO> result);

    @Mappings({})
    List<AdminRoleVO> convert(List<RoleBO> roleList);

    @Mappings({})
    PageResult<AdminVO> convertAdminVOPage(PageResult<AdminBO> page);

    @Mappings({})
    List<AdminVO.Role> convertAdminVORoleList(Collection<RoleBO> list);

}
