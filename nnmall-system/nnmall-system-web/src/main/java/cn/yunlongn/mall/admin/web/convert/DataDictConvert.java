package cn.yunlongn.mall.admin.web.convert;

import cn.yunlongn.mall.admin.api.bo.datadict.DataDictBO;
import cn.yunlongn.mall.admin.web.vo.DataDictEnumVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.List;

/**
 * @author Yun
 */
@Mapper
public interface DataDictConvert {
    DataDictConvert INSTANCE = Mappers.getMapper(DataDictConvert.class);

    @Mappings({})
    List<DataDictEnumVO.Value> convert2(List<DataDictBO> dataDictBO);
}
