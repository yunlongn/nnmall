package cn.yunlongn.mall.admin.web.controller.admins;


import cn.yunlongn.mall.admin.api.bo.datadict.DataDictBO;
import cn.yunlongn.mall.admin.api.dto.datadict.DataDictAddDTO;
import cn.yunlongn.mall.admin.api.dto.datadict.DataDictUpdateDTO;
import cn.yunlongn.mall.admin.api.fegin.DataDictServiceFeign;
import cn.yunlongn.mall.admin.sdk.annotation.RequiresPermissions;
import cn.yunlongn.mall.admin.sdk.context.AdminSecurityContextHolder;
import cn.yunlongn.mall.admin.web.convert.DataDictConvert;
import cn.yunlongn.mall.admin.web.vo.DataDictEnumVO;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static cn.yunlongn.mall.common.core.vo.CommonResult.success;


/**
 * @author Yun
 */
@RestController
@RequestMapping("admins/data_dict")
@Api("数据字典模块")
public class DataDictController {

    @Autowired
    private DataDictServiceFeign dataDictServiceFeign;

    @GetMapping("/list")
    @ApiOperation(value = "数据字典全列表")
    @RequiresPermissions("system.dataDict.list")
    public CommonResult<List<DataDictBO>> list() {
        return success( dataDictServiceFeign.selectDataDictList());
    }

    @GetMapping("/tree")
    @RequiresPermissions({}) // 因为是通用的接口，所以无需权限标识
    @ApiOperation(value = "数据字典树结构", notes = "该接口返回的信息更为精简。一般用于前端缓存数据字典到本地。")
    public CommonResult<List<DataDictEnumVO>> tree() {
        // 查询数据字典全列表
        List<DataDictBO> dataDicts = dataDictServiceFeign.selectDataDictList();
        // 构建基于 enumValue 聚合的 Multimap
        // KEY 是 enumValue ，VALUE 是 DataDictBO 数组
        ImmutableListMultimap<String, DataDictBO> dataDictMap = Multimaps.index(dataDicts, DataDictBO::getEnumValue);
        // 构建返回结果
        List<DataDictEnumVO> dataDictEnumVOs = new ArrayList<>(dataDictMap.size());
        dataDictMap.keys().forEach(enumValue -> {
            DataDictEnumVO dataDictEnumVO = new DataDictEnumVO().setEnumValue(enumValue)
                    .setValues(DataDictConvert.INSTANCE.convert2(dataDictMap.get(enumValue)));
            dataDictEnumVOs.add(dataDictEnumVO);
        });
        return success(dataDictEnumVOs);
    }

    @PostMapping("/add")
    @RequiresPermissions("system.dataDict.add")
    @ApiOperation(value = "创建数据字典")
    public CommonResult<DataDictBO> add(DataDictAddDTO dataDictAddDTO) {
        return success(dataDictServiceFeign.addDataDict(AdminSecurityContextHolder.getContext().getAdminId(), dataDictAddDTO));
    }

    @PostMapping("/update")
    @RequiresPermissions("system.dataDict.update")
    @ApiOperation(value = "更新数据字典")
    public CommonResult<Boolean> update(DataDictUpdateDTO dataDictUpdateDTO) {
        return success(dataDictServiceFeign.updateDataDict(AdminSecurityContextHolder.getContext().getAdminId(), dataDictUpdateDTO));
    }

    @PostMapping("/delete")
    @RequiresPermissions("system.dataDict.delete")
    @ApiOperation(value = "删除数据字典")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "100")
    public CommonResult<Boolean> delete(@RequestParam("id") Integer id) {
        return success(dataDictServiceFeign.deleteDataDict(AdminSecurityContextHolder.getContext().getAdminId(), id));
    }



}
