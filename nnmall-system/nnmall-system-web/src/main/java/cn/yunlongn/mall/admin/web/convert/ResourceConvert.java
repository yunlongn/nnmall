package cn.yunlongn.mall.admin.web.convert;


import cn.yunlongn.mall.admin.api.bo.resource.ResourceBO;
import cn.yunlongn.mall.admin.web.convert.resource.ResourceTreeNodeVO;
import cn.yunlongn.mall.admin.web.convert.resource.ResourceVO;
import cn.yunlongn.mall.admin.web.convert.role.RoleResourceTreeNodeVO;
import cn.yunlongn.mall.admin.web.vo.AdminMenuTreeNodeVO;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ResourceConvert {

    ResourceConvert INSTANCE = Mappers.getMapper(ResourceConvert.class);

    @Mappings({})
    AdminMenuTreeNodeVO convert(ResourceBO resourceBO);

    @Mappings({})
    ResourceTreeNodeVO convert2(ResourceBO resourceBO);

    @Mappings({})
    RoleResourceTreeNodeVO convert4(ResourceBO resourceBO);

    @Mappings({})
    ResourceVO convert3(ResourceBO resourceBO);

    @Mappings({})
    CommonResult<ResourceVO> convert3(CommonResult<ResourceBO> resourceBO);

}
