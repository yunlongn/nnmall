package cn.yunlongn.mall.admin.web.controller.admins;

import cn.yunlongn.mall.admin.api.bo.admin.AdminAuthenticationBO;
import cn.yunlongn.mall.admin.api.bo.oauth2.OAuth2AuthenticationBO;
import cn.yunlongn.mall.admin.api.dto.admin.AdminAuthenticationDTO;
import cn.yunlongn.mall.admin.api.dto.oauth2.OAuth2GetTokenDTO;
import cn.yunlongn.mall.admin.api.fegin.AdminServiceFeign;
import cn.yunlongn.mall.admin.api.fegin.OAuth2ServiceFeign;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static cn.yunlongn.mall.common.core.vo.CommonResult.success;

/**
 * @author Yun
 */
@RestController
@RequestMapping("admins/passport")
@Api("Admin Passport 模块")
public class PassportController {

    /**
     * 登陆总数 Metrics 预留计算  为以后增加埋点
     */
    private static final Counter METRICS_LOGIN_TOTAL = Metrics.counter("mall.admin.passport.login.total");

    @Autowired
    private OAuth2ServiceFeign oAuth2ServiceFeign;

    @Autowired
    private AdminServiceFeign adminServiceFeign;


    @PostMapping("/login")
    @ApiOperation(value = "账号 + 密码登陆")
    public CommonResult<AdminAuthenticationBO> login(@Validated @RequestBody AdminAuthenticationDTO adminAuthenticationDTO) {
        // 增加计数
        METRICS_LOGIN_TOTAL.increment();
        // 执行登陆
        return success(adminServiceFeign.authentication(adminAuthenticationDTO));
    }


    @PostMapping("/token")
    @ApiOperation(value = "账号 + 密码登陆")
    public OAuth2AuthenticationBO getAuthentication(@Validated @RequestBody OAuth2GetTokenDTO oauth2GetTokenDTO) {
        return oAuth2ServiceFeign.getAuthentication(oauth2GetTokenDTO);
    }



    // TODO 功能 logout

    // TODO 功能 refresh_token

}
