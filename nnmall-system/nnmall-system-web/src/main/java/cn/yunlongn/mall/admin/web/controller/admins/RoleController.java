package cn.yunlongn.mall.admin.web.controller.admins;

import cn.yunlongn.mall.admin.api.bo.resource.ResourceBO;
import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.dto.role.RoleAddDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleAssignResourceDTO;
import cn.yunlongn.mall.admin.api.dto.role.RolePageDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleUpdateDTO;
import cn.yunlongn.mall.admin.api.fegin.ResourceServiceFeign;
import cn.yunlongn.mall.admin.api.fegin.RoleServiceFeign;
import cn.yunlongn.mall.admin.sdk.context.AdminSecurityContextHolder;
import cn.yunlongn.mall.admin.web.convert.ResourceConvert;
import cn.yunlongn.mall.admin.web.convert.role.RoleResourceTreeNodeVO;
import cn.yunlongn.mall.common.core.constant.ResourceConstants;
import cn.yunlongn.mall.common.core.util.CollectionUtil;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import cn.yunlongn.mall.common.core.vo.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static cn.yunlongn.mall.common.core.vo.CommonResult.success;


@RestController
@RequestMapping("admins/role")
@Api("角色模块")
public class RoleController {

    @Autowired
    private RoleServiceFeign roleServiceFeign;

    @Autowired
    private ResourceServiceFeign resourceServiceFeign;

    @GetMapping("/page")
    @ApiOperation(value = "角色分页")
    public CommonResult<PageResult<RoleBO>> page(RolePageDTO rolePageDTO) {
        return success(roleServiceFeign.getRolePage(rolePageDTO));
    }

    @PostMapping("/add")
    @ApiOperation(value = "创建角色")
    public CommonResult<RoleBO> add(@RequestBody RoleAddDTO roleAddDTO) {
        return success(roleServiceFeign.addRole(AdminSecurityContextHolder.getContext().getAdminId(), roleAddDTO));
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新角色")
    public CommonResult<Boolean> update(@RequestBody RoleUpdateDTO roleUpdateDTO) {
        return success(roleServiceFeign.updateRole(AdminSecurityContextHolder.getContext().getAdminId(), roleUpdateDTO));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除角色")
    @ApiImplicitParam(name = "id", value = "角色编号", required = true, example = "1")
    public CommonResult<Boolean> delete(@PathVariable("id") Integer id) {
        return success(roleServiceFeign.deleteRole(AdminSecurityContextHolder.getContext().getAdminId(), id));
    }

    @SuppressWarnings("Duplicates")
    @GetMapping("/resource_tree")
    @ApiOperation(value = "获得角色拥有的菜单权限", notes = "以树结构返回")
    @ApiImplicitParam(name = "id", value = "角色编号", required = true, example = "1")
    public CommonResult<Map> resourceTree(@RequestParam("id") Integer id) {
        // 此处，严格来说可以在校验下角色是否存在。不过呢，校验了也没啥意义，因为一般不存在这个情况，且不会有业务上的影响。并且，反倒多了一次 rpc 调用。
        // 第一步，获得角色拥有的资源数组
        // 获取用户用有哪一些资源 然后最后去设置
        Set<Integer> roleResources = resourceServiceFeign.getResourcesByTypeAndRoleIds(null, CollectionUtil.asSet(id))
                .stream().map(ResourceBO::getId).collect(Collectors.toSet());
        // 第二步，获得资源树
        List<ResourceBO> allResources = resourceServiceFeign.getResourcesByType(null);
        // 创建 AdminMenuTreeNodeVO Map
        Map<Integer, RoleResourceTreeNodeVO> treeNodeMap = allResources.stream().collect(Collectors.toMap(ResourceBO::getId, ResourceConvert.INSTANCE::convert4));
        // 处理父子关系
        treeNodeMap.values().stream()
                .filter(node -> !node.getPid().equals(ResourceConstants.PID_ROOT))
                .forEach((childNode) -> {
                    // 获得父节点
                    RoleResourceTreeNodeVO parentNode = treeNodeMap.get(childNode.getPid());
                    if (parentNode.getChildren() == null) { // 初始化 children 数组
                        parentNode.setChildren(new ArrayList<>());
                    }
                    // 将自己添加到父节点中
                    parentNode.getChildren().add(childNode);
                });
        // 获得到所有的根节点
        List<RoleResourceTreeNodeVO> rootNodes = treeNodeMap.values().stream()
                .filter(node -> node.getPid().equals(ResourceConstants.PID_ROOT))
                .sorted(Comparator.comparing(RoleResourceTreeNodeVO::getSort))
                .collect(Collectors.toList());
        // 第三步，设置角色是否有该角色
        treeNodeMap.values().forEach(nodeVO -> nodeVO.setAssigned(roleResources.contains(nodeVO.getId())));
        Map<String, Object> map = new HashMap<>(2);
        map.put("list", rootNodes);
        map.put("defaultChecked", roleResources);
        // 返回结果
        return success(map);
    }

    @PostMapping("/assign_resource")
    @ApiOperation(value = "分配角色资源")
    public CommonResult<Boolean> assignResource(@RequestBody RoleAssignResourceDTO roleAssignResourceDTO) {
        return success(roleServiceFeign.assignRoleResource(AdminSecurityContextHolder.getContext().getAdminId(), roleAssignResourceDTO));
    }

}
