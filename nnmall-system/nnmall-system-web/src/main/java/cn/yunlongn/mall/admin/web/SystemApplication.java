package cn.yunlongn.mall.admin.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Yun
 */
@SpringBootApplication(scanBasePackages = {
        "cn.yunlongn.mall.admin",
        "cn.yunlongn.mall.spring.core"})
@EnableAsync(proxyTargetClass = true)
@EnableEurekaClient
@EnableFeignClients(basePackages = {"cn.yunlongn.mall.admin"})
public class SystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
    }

}
