package cn.yunlongn.mall.admin.web.controller.admins;

import cn.yunlongn.mall.admin.api.bo.admin.AdminBO;
import cn.yunlongn.mall.admin.api.bo.resource.ResourceBO;
import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.dto.admin.*;
import cn.yunlongn.mall.admin.api.fegin.AdminServiceFeign;
import cn.yunlongn.mall.admin.api.fegin.ResourceServiceFeign;
import cn.yunlongn.mall.admin.api.fegin.RoleServiceFeign;
import cn.yunlongn.mall.admin.sdk.annotation.RequiresPermissions;
import cn.yunlongn.mall.admin.sdk.context.AdminSecurityContextHolder;
import cn.yunlongn.mall.admin.web.convert.AdminConvert;
import cn.yunlongn.mall.admin.web.convert.ResourceConvert;
import cn.yunlongn.mall.admin.web.vo.AdminMenuTreeNodeVO;
import cn.yunlongn.mall.admin.web.vo.admin.AdminRoleVO;
import cn.yunlongn.mall.admin.web.vo.admin.AdminVO;
import cn.yunlongn.mall.common.core.constant.MallConstants;
import cn.yunlongn.mall.common.core.constant.ResourceConstants;
import cn.yunlongn.mall.common.core.util.CollectionUtil;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import cn.yunlongn.mall.common.core.vo.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static cn.yunlongn.mall.common.core.vo.CommonResult.success;


@RestController
@RequestMapping(MallConstants.ROOT_PATH_ADMIN + "/admin")
@Api("管理员模块")
public class AdminController {

    @Autowired
    AdminServiceFeign adminServiceFeign;
    @Autowired
    ResourceServiceFeign resourceServiceFeign;
    @Autowired
    RoleServiceFeign roleServiceFeign;

    // =========== 当前管理员相关的资源 API ===========

    // TODO 功能：当前管理员
    @SuppressWarnings("Duplicates")
    @GetMapping("/menu_resource_tree")
    @ApiOperation(value = "获得当前登陆的管理员拥有的菜单权限", notes = "以树结构返回")
    public CommonResult<List<AdminMenuTreeNodeVO>> menuResourceTree() {
        List<ResourceBO> resources = resourceServiceFeign.getResourcesByTypeAndRoleIds(ResourceConstants.TYPE_MENU,
                AdminSecurityContextHolder.getContext().getRoleIds());

        // 创建 AdminMenuTreeNodeVO Map
        // 使用 LinkedHashMap 的原因，是为了排序 。实际也可以用 Stream API ，就是太丑了。
        Map<Integer, AdminMenuTreeNodeVO> treeNodeMap = new LinkedHashMap<>();
        resources.stream().sorted(Comparator.comparing(ResourceBO::getSort)).forEach(resourceBO -> treeNodeMap.put(resourceBO.getId(), ResourceConvert.INSTANCE.convert(resourceBO)));
        // 处理父子关系
        treeNodeMap.values().stream()
                .filter(node -> !node.getPid().equals(ResourceConstants.PID_ROOT))
                .forEach((childNode) -> {
                    // 获得父节点
                    AdminMenuTreeNodeVO parentNode = treeNodeMap.get(childNode.getPid());
                    // 初始化 children 数组
                    if (parentNode.getChildren() == null) {
                        parentNode.setChildren(new ArrayList<>());
                    }
                    // 将自己添加到父节点中
                    parentNode.getChildren().add(childNode);
                });

        // 获得到所有的根节点
        List<AdminMenuTreeNodeVO> rootNodes = treeNodeMap.values().stream()
                .filter(node -> node.getPid().equals(ResourceConstants.PID_ROOT))
//                .sorted(Comparator.comparing(AdminMenuTreeNodeVO::getSort))
                .collect(Collectors.toList());
        return success(rootNodes);
    }
    @GetMapping("/url_resource_list")
    @ApiOperation(value = "获得当前登陆的管理员拥有的 URL 权限列表")
    public CommonResult<Set<String>> urlResourceList() {
        List<ResourceBO> resources = resourceServiceFeign.getResourcesByTypeAndRoleIds(ResourceConstants.TYPE_BUTTON, AdminSecurityContextHolder.getContext().getRoleIds());
        return success(resources.stream().map(ResourceBO::getHandler).collect(Collectors.toSet()));
    }

    // =========== 管理员管理 API ===========

    @GetMapping("/page")
    @RequiresPermissions("system.admin.page")
    @ApiOperation(value = "管理员分页")
    public CommonResult<PageResult<AdminVO>> page(AdminPageDTO adminPageDTO) {
//        System.out.println(adminPageDTO);
//        System.out.println(adminPageDTO);
//        System.out.println(adminPageDTO.getPageNo());
        PageResult<AdminBO> page = adminServiceFeign.getAdminPage(adminPageDTO);
        System.out.println(page.getList());
        PageResult<AdminVO> resultPage = AdminConvert.INSTANCE.convertAdminVOPage(page);
        // 拼接结果

        if (resultPage.getList() != null && !resultPage.getList().isEmpty()) {
            // 查询角色数组
            Map<Integer, Collection<RoleBO>> roleMap = adminServiceFeign.getAdminRolesMap(CollectionUtil.convertList(resultPage.getList(), AdminBO::getId));
//            System.out.println();
            resultPage.getList().forEach(admin -> admin.setRoles(AdminConvert.INSTANCE.convertAdminVORoleList(roleMap.get(admin.getId()))));
        } else {
            resultPage.setList(Collections.emptyList());
        }


        return success(resultPage);
    }

    @PostMapping("/add")
    @ApiOperation(value = "创建管理员")
    public CommonResult<AdminBO> add(@Validated @RequestBody AdminAddDTO adminAddDTO) {

        return success(adminServiceFeign.addAdmin(AdminSecurityContextHolder.getContext().getAdminId(), adminAddDTO));
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新管理员")
    public CommonResult<Boolean> update(@Validated @RequestBody AdminUpdateDTO adminUpdateDTO) {
        return success("修改成功",adminServiceFeign.updateAdmin( AdminSecurityContextHolder.getContext().getAdminId(), adminUpdateDTO));
    }

    @PostMapping("/update_status")
    @ApiOperation(value = "更新管理员状态")
    public CommonResult<Boolean> updateStatus(@Validated @RequestBody AdminUpdateStatusDTO adminUpdateStatusDTO) {
        return success(adminServiceFeign.updateAdminStatus(AdminSecurityContextHolder.getContext().getAdminId(), adminUpdateStatusDTO));
    }

    @GetMapping("/role_list")
    @ApiOperation(value = "指定管理员拥有的角色列表")
    @ApiImplicitParam(name = "id", value = "管理员编号", required = true, example = "1")
    public CommonResult<List<AdminRoleVO>> roleList(@RequestParam("id") Integer id) {
        // 获得所有角色列表
        List<RoleBO> allRoleList = roleServiceFeign.getRoleList();
        // 获得管理员的角色数组
        Set<Integer> adminRoleIdSet = CollectionUtil.convertSet(adminServiceFeign.getRoleList(id), RoleBO::getId);
        // 转换出返回结果
        List<AdminRoleVO> result = AdminConvert.INSTANCE.convert(allRoleList);
        // 设置每个角色是否赋予给改管理员
        result.forEach(adminRoleVO -> adminRoleVO.setAssigned(adminRoleIdSet.contains(adminRoleVO.getId())));
        return success(result);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除管理员")
    @ApiImplicitParam(name = "id", value = "管理员编号", required = true, example = "1")
    public CommonResult<Boolean> delete(@RequestParam("id") Integer id) {
        return success(adminServiceFeign.deleteAdmin(AdminSecurityContextHolder.getContext().getAdminId(), id));
    }

    @PostMapping("/assign_role")
    @ApiOperation(value = "分配给管理员角色")
    public CommonResult<Boolean> assignRole(@Validated @RequestBody AdminAssignRoleDTO adminAssignRoleDTO) {
        return success(adminServiceFeign.assignAdminRole(AdminSecurityContextHolder.getContext().getAdminId(), adminAssignRoleDTO));
    }
}
