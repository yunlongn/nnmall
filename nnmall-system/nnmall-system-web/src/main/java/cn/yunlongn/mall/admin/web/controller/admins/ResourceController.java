package cn.yunlongn.mall.admin.web.controller.admins;


import cn.yunlongn.mall.admin.api.bo.resource.ResourceBO;
import cn.yunlongn.mall.admin.api.dto.resource.ResourceAddDTO;
import cn.yunlongn.mall.admin.api.dto.resource.ResourceUpdateDTO;
import cn.yunlongn.mall.admin.api.fegin.ResourceServiceFeign;
import cn.yunlongn.mall.admin.sdk.context.AdminSecurityContextHolder;
import cn.yunlongn.mall.admin.web.convert.ResourceConvert;
import cn.yunlongn.mall.admin.web.convert.resource.ResourceTreeNodeVO;
import cn.yunlongn.mall.common.core.constant.ResourceConstants;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cn.yunlongn.mall.common.core.vo.CommonResult.success;


@RestController
@RequestMapping("admins/resource")
@Api("资源模块")
public class ResourceController {

    @Autowired
    private ResourceServiceFeign resourceServiceFeign;

    @SuppressWarnings("Duplicates")
    @GetMapping("/tree")
    @ApiOperation(value = "获得所有资源，按照树形结构返回")
    public CommonResult<List<ResourceTreeNodeVO>> tree() {
        List<ResourceBO> resources = resourceServiceFeign.getResourcesByType(null);
        // 创建 AdminMenuTreeNodeVO Map
        // ResourceConvert.INSTANCE::convert2 将每一个ResourceBO对象转换成ResourceTreeNodeVO
        Map<Integer, ResourceTreeNodeVO> treeNodeMap = resources.stream().collect(Collectors.toMap(ResourceBO::getId, ResourceConvert.INSTANCE::convert2));
        // 处理父子关系
        treeNodeMap.values().stream()
                // 得到父节点不是0的元素
                .filter(node -> !node.getPid().equals(ResourceConstants.PID_ROOT))
                .forEach((childNode) -> {
                    // 遍历元素， 然后从map对象中拿到这个元素的父级元素  将自己添加进入这个父级元素的children 当中
                    ResourceTreeNodeVO parentNode = treeNodeMap.get(childNode.getPid());
                    // 初始化 children 数组 如果这里面为空就初始化list对象
                    if (parentNode.getChildren() == null) {
                        parentNode.setChildren(new ArrayList<>());
                    }
                    // 将自己添加到父节点中
                    parentNode.getChildren().add(childNode);
                });


        // 获得到所有的根节点
        List<ResourceTreeNodeVO> rootNodes = treeNodeMap.values().stream()
                .filter(node -> node.getPid().equals(ResourceConstants.PID_ROOT))
                // 根据 sort 排序
                .sorted(Comparator.comparing(ResourceTreeNodeVO::getSort))
                .collect(Collectors.toList());
        return success(rootNodes);
    }

    @PostMapping("/add")
    @ApiOperation(value = "创建资源", notes = "例如说，菜单资源，Url 资源")
    public CommonResult<ResourceBO> add(ResourceAddDTO resourceAddDTO) {
        return success(resourceServiceFeign.addResource(AdminSecurityContextHolder.getContext().getAdminId(), resourceAddDTO));
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新资源")
    public CommonResult<Boolean> update(ResourceUpdateDTO resourceUpdateDTO) {
        return success(resourceServiceFeign.updateResource(AdminSecurityContextHolder.getContext().getAdminId(), resourceUpdateDTO));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除资源")
    @ApiImplicitParam(name = "id", value = "资源编号", required = true, example = "1")
    public CommonResult<Boolean> delete(@RequestParam("id") Integer id) {
        return success(resourceServiceFeign.deleteResource(AdminSecurityContextHolder.getContext().getAdminId(), id));
    }

}
