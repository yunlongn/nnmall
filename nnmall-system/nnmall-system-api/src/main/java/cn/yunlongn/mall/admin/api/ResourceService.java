package cn.yunlongn.mall.admin.api;


import cn.yunlongn.mall.admin.api.bo.resource.ResourceBO;
import cn.yunlongn.mall.admin.api.dto.resource.ResourceAddDTO;
import cn.yunlongn.mall.admin.api.dto.resource.ResourceUpdateDTO;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

public interface ResourceService {

    /**
     * 查询指定类型 + 指定角色的资源列表
     *
     * @param type    指定类型。可以为空，此时不作为过滤条件
     * @param roleIds 指定角色的数组。
     * @return 资源列表
     */
    @PostMapping("getResourcesByTypeAndRoleIds")
    List<ResourceBO> getResourcesByTypeAndRoleIds(@RequestParam @Nullable Integer type,@RequestBody Set<Integer> roleIds);

    /**
     * 查询指定类型的资源列表
     * 获取菜单或者 按钮的指定资源
     * @param type 指定类型。可以为空，此时不做为过滤条件
     * @return 资源列表
     */
    @PostMapping("getResourcesByType")
    List<ResourceBO> getResourcesByType(@RequestParam @Nullable Integer type);

    /**
     * 添加新资源
     * @return 刚添加的资源
     */
    @PostMapping("addResource")
    ResourceBO addResource(@RequestParam Integer adminId,@RequestBody ResourceAddDTO resourceAddDTO);

    @PostMapping("updateResource")
    Boolean updateResource(@RequestParam Integer adminId,@RequestBody ResourceUpdateDTO resourceUpdateDTO);

    @PostMapping("deleteResource")
    Boolean deleteResource(@RequestParam Integer adminId,@RequestBody Integer resourceId);



}
