package cn.yunlongn.mall.admin.api;

import cn.yunlongn.mall.admin.api.bo.admin.AdminAuthenticationBO;
import cn.yunlongn.mall.admin.api.bo.admin.AdminAuthorizationBO;
import cn.yunlongn.mall.admin.api.bo.admin.AdminBO;
import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.dto.admin.*;
import cn.yunlongn.mall.common.core.vo.PageResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 管理员 Service 接口
 */

public interface AdminService {

    /**
     * 管理员认证。认证成功后，返回认证信息
     * 实际上，就是用户名 + 密码登陆
     * 用户登录成功之后返回一个Token
     * @param adminAuthenticationDTO 用户认证信息
     * @return 认证信息
     */
    @PostMapping("authentication")
    AdminAuthenticationBO authentication(@RequestBody AdminAuthenticationDTO adminAuthenticationDTO);

    @PostMapping("getAdminPage")
    PageResult<AdminBO> getAdminPage(@RequestBody @Validated AdminPageDTO adminPageDTO);

    @PostMapping("addAdmin")
    AdminBO addAdmin(@RequestParam Integer adminId, @RequestBody @Validated AdminAddDTO adminAddDTO);

    @PostMapping("updateAdmin")
    Boolean updateAdmin(@RequestParam Integer adminId, @RequestBody @Validated AdminUpdateDTO adminUpdateDTO);

    @PostMapping("updateAdminStatus")
    Boolean updateAdminStatus(@RequestParam Integer adminId, @Validated @RequestBody AdminUpdateStatusDTO adminUpdateStatusDTO);

    @PostMapping("deleteAdmin")
    Boolean deleteAdmin(@RequestParam Integer adminId, @Validated @RequestBody Integer updateAdminId);

    @PostMapping("assignAdminRole")
    public Boolean assignAdminRole(@RequestParam Integer adminId, @Validated @RequestBody AdminAssignRoleDTO adminAssignRoleDTO);



    /**
     * 批量查询每个管理员拥有的角色
     *
     * @param adminIds 管理员编号数组
     * @return 每个管理员拥有的角色
     */
    @PostMapping("getAdminRolesMap")
    Map<Integer, Collection<RoleBO>> getAdminRolesMap(@Validated @RequestBody Collection<Integer> adminIds);
    /**
     * 获得指定管理员拥有的角色数组
     *
     * @param adminId 指定管理员
     * @return 角色编号数组
     */
    @PostMapping("getAdminRoleList")
    List<RoleBO> getRoleList(@RequestParam Integer adminId);


    /**
     * 判断管理员是否有指定权限
     *
     * @param adminId 管理员
     * @param permissions 权限数组
     * @return 管理员授权信息
     */
    @PostMapping("checkPermissions")
    AdminAuthorizationBO checkPermissions(@RequestParam Integer adminId, @RequestBody(required = false) List<String> permissions);

}
