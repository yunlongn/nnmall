package cn.yunlongn.mall.admin.api;

import cn.yunlongn.mall.admin.api.bo.oauth2.OAuth2AccessTokenBO;
import cn.yunlongn.mall.admin.api.bo.oauth2.OAuth2AuthenticationBO;
import cn.yunlongn.mall.admin.api.dto.oauth2.OAuth2CreateTokenDTO;
import cn.yunlongn.mall.admin.api.dto.oauth2.OAuth2GetTokenDTO;
import cn.yunlongn.mall.admin.api.dto.oauth2.OAuth2RemoveTokenByUserDTO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Oauth2 服务接口
 */

public interface OAuth2Service {

    /**
     * 根据身份信息，创建 accessToken 信息
     *
     * @param oauth2CreateTokenDTO 身份信息 DTO
     * @return accessToken 信息
     */
    @PostMapping("createToken")
    OAuth2AccessTokenBO createToken(@Validated @RequestBody OAuth2CreateTokenDTO oauth2CreateTokenDTO);

    /**
     * 从请求投获取的token来这里
     * 通过 accessToken 获得身份信息
     * @param oauth2GetTokenDTO accessToken 信息
     * @return 身份信息
     */
    @PostMapping("getAuthentication")
    OAuth2AuthenticationBO getAuthentication(@Validated @RequestBody OAuth2GetTokenDTO oauth2GetTokenDTO);

    @PostMapping("removeToken")
    public void removeToken(@Validated @RequestBody OAuth2RemoveTokenByUserDTO oauth2RemoveTokenByUserDTO);
}
