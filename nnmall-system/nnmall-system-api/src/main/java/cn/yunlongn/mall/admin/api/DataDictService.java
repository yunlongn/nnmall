package cn.yunlongn.mall.admin.api;


import cn.yunlongn.mall.admin.api.bo.datadict.DataDictBO;
import cn.yunlongn.mall.admin.api.dto.datadict.DataDictAddDTO;
import cn.yunlongn.mall.admin.api.dto.datadict.DataDictUpdateDTO;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;

public interface DataDictService {

    @RequestMapping("selectDataDictList")
    public List<DataDictBO> selectDataDictList();

    @RequestMapping("addDataDict")
    DataDictBO addDataDict(@RequestParam Integer adminId,@RequestBody DataDictAddDTO dataDictAddDTO);

    @RequestMapping("updateDataDict")
    public Boolean updateDataDict(@RequestParam Integer adminId,@RequestBody DataDictUpdateDTO dataDictUpdateDTO);

    @RequestMapping("deleteDataDict")
    public Boolean deleteDataDict(@RequestParam Integer adminId,@RequestBody Integer dataDictId);


    /**
     * 返回指定枚举值的字典对象
     * @param dictKey 大类枚举值
     * @return 字典列表对象
     */
    @RequestMapping("getDataDict")
    public CommonResult<DataDictBO> getDataDict(@RequestParam String dictKey, @RequestBody Object dictValue);


    /**
     * 返回对应大枚值的所有子类列表
     * @param dictKey 大类枚举值
     * @return 字典列表对象
     */
    @RequestMapping("getDataDictKeyList")
    public CommonResult<List<DataDictBO>> getDataDict(@RequestParam String dictKey);


    /**
     * 返回对应小枚举值的的所有字典对象
     * @param dictKey 大类枚举值
     * @return 字典列表对象
     */
    @RequestMapping("getDataDictList")
    public CommonResult<List<DataDictBO>> getDataDictList(@RequestParam String dictKey, @RequestBody Collection<?> dictValueList);


}
