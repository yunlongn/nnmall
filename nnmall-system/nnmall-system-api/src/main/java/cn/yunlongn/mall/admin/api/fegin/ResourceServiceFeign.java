package cn.yunlongn.mall.admin.api.fegin;

import cn.yunlongn.mall.admin.api.OAuth2Service;
import cn.yunlongn.mall.admin.api.ResourceService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 资源 服务接口
 */
@FeignClient(name = "admin-server", path = "/admin-server/feign")
public interface ResourceServiceFeign extends ResourceService {


}
