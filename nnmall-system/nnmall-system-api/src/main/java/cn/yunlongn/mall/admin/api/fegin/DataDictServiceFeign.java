package cn.yunlongn.mall.admin.api.fegin;

import cn.yunlongn.mall.admin.api.DataDictService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 字典 服务接口
 */
@FeignClient(name = "admin-server", path = "/admin-server/feign")
public interface DataDictServiceFeign extends DataDictService {

}
