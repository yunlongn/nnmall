package cn.yunlongn.mall.admin.api;

import cn.yunlongn.mall.admin.api.bo.role.RoleBO;
import cn.yunlongn.mall.admin.api.dto.role.RoleAddDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleAssignResourceDTO;
import cn.yunlongn.mall.admin.api.dto.role.RolePageDTO;
import cn.yunlongn.mall.admin.api.dto.role.RoleUpdateDTO;
import cn.yunlongn.mall.common.core.vo.PageResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;

public interface RoleService {


    @PostMapping("getRolePage")
    PageResult<RoleBO> getRolePage(@RequestBody RolePageDTO rolePageDTO);

    /**
     * @return 返回角色列表
     */
    @PostMapping("getRoleList")
    List<RoleBO> getRoleList();

    @PostMapping("getRoleListIds")
    List<RoleBO> getRoleList(@RequestBody Collection<Integer> ids);

    @PostMapping("addRole")
    RoleBO addRole(@RequestParam Integer adminId, @RequestBody RoleAddDTO roleAddDTO);

    @PostMapping("updateRole")
    Boolean updateRole(@RequestParam Integer adminId, @RequestBody RoleUpdateDTO roleUpdateDTO);

    @PostMapping("deleteRole")
    Boolean deleteRole(@RequestParam Integer adminId,@RequestParam Integer roleId);


    /****
     * 分配角色路由资源。。让角色绑定资源信息
     * @param adminId 当前用户id 备用
     * @param roleAssignResourceDTO 角色id 和要分配的资源id集合
     * @return
     */
    @PostMapping("assignRoleResource")
    Boolean assignRoleResource(@RequestParam Integer adminId, @RequestBody RoleAssignResourceDTO roleAssignResourceDTO);

}
