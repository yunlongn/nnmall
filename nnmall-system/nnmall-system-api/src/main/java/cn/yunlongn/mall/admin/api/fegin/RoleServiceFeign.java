package cn.yunlongn.mall.admin.api.fegin;

import cn.yunlongn.mall.admin.api.ResourceService;
import cn.yunlongn.mall.admin.api.RoleService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 资源 服务接口
 * @author Yun
 */
@FeignClient(name = "admin-server", path = "/admin-server/feign")
public interface RoleServiceFeign extends RoleService {


}
