package cn.yunlongn.mall.admin.api.fegin;

import cn.yunlongn.mall.admin.api.OAuth2Service;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * Oauth2 服务接口
 */
@FeignClient(name = "admin-server", path = "/admin-server/feign")
public interface OAuth2ServiceFeign extends OAuth2Service {

}
