package cn.yunlongn.mall.admin.api.fegin;

import cn.yunlongn.mall.admin.api.AdminService;
import org.springframework.cloud.openfeign.FeignClient;


/**
 * 管理员 Service 接口
 */
@FeignClient(name = "admin-server", path = "/admin-server/feign")
public interface AdminServiceFeign extends AdminService{



}
