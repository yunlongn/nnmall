/**
 * 提供 SDK 给其它服务，使用如下功能：
 *
 * 1. 通过 {@link cn.yunlongn.mall.admin.sdk.interceptor.AdminSecurityInterceptor} 拦截器，实现需要登陆 URL 的鉴权
 */
package cn.yunlongn.mall.admin.sdk;
