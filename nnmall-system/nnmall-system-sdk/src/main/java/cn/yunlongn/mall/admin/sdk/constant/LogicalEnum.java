package cn.yunlongn.mall.admin.sdk.constant;

/**
 * 逻辑类型枚举
 * @author Yun
 */
public enum LogicalEnum {

    /**
     * 并且
     */
    AND,
    /**
     * 或者
     */
    OR,

}
