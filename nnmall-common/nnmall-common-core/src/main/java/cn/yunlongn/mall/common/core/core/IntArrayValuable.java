package cn.yunlongn.mall.common.core.core;

/**
 * 可生成 Int 数组的接口
 * @author Yun
 */
public interface IntArrayValuable {

    /**
     * @return int 数组
     */
    int[] array();

}
