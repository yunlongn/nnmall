package cn.yunlongn.mall.spring.core.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter4;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MessageConverterConfig {

    @Bean
    public FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
        FastJsonHttpMessageConverterExtension converter = new FastJsonHttpMessageConverterExtension();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(
                SerializerFeature.PrettyFormat,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteNullListAsEmpty,
                // 解决重复引用 出现$.body[0]的问题
                SerializerFeature.DisableCircularReferenceDetect
        );
        fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
        // 将null装换为""
        ValueFilter valueFilter = new ValueFilter() {
            public Object process(Object o, String s, Object o1) {
                if (null == o1) {
                    o1 = "";
                }
//                System.out.println(s);

//                 自定义注解拦截器。   拦截反序列化
//                try {
//                    Field field = o.getClass().getSuperclass().getDeclaredField(s);
//                    ConvertDisplay convertDisplay = TypeUtils.getAnnotation(field, ConvertDisplay.class);
//
//                    if (ToolUtil.isNotEmpty(convertDisplay)) {
//
//                    }
//                    System.out.println(convertDisplay);
//                } catch (Exception e) {
////                    e.printStackTrace();
////                    System.out.println("eeeeeeeeeeee");
//                }

                return o1;
            }
        };
        fastJsonConfig.setSerializeFilters(valueFilter);
//        fastJsonConfig.
        //新增
//        fastJsonConfig.setSerializerFeatures(SerializerFeature.DisableCircularReferenceDetect);
//        fastJsonConfig.setSerializerFeatures(SerializerFeature.WriteNullListAsEmpty);

        converter.setFastJsonConfig(fastJsonConfig);
//        fastJsonConfig.setFeatures(SerializerFeature.DisableCircularReferenceDetect);

        return converter;
    }

    /***
     * 配置只有以下两个格式才会进入fastJson序列化
     */
    public class FastJsonHttpMessageConverterExtension extends FastJsonHttpMessageConverter {
        FastJsonHttpMessageConverterExtension() {
            List<MediaType> mediaTypes = new ArrayList<>();
            mediaTypes.add(MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            mediaTypes.add(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8"));

            // 只有以上类型的数据才要走序列化 就是才会配置上面的东西 https://www.cnblogs.com/page12/p/8166935.html
            setSupportedMediaTypes(mediaTypes);
        }
    }
}
