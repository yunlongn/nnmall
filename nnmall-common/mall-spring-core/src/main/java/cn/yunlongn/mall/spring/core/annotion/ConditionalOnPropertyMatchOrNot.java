package cn.yunlongn.mall.spring.core.annotion;


import cn.yunlongn.mall.spring.core.aop.OnPropertyMatchOrNot;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * @author Yun
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
@Documented
@Conditional(OnPropertyMatchOrNot.class)
public @interface ConditionalOnPropertyMatchOrNot {
    String property1() default "";

    String property2() default "";

    String equ() default "y";

    /**
     * 不等于这个值的时候就允许创建bean容器
     */
    String value() default "";
}
