package cn.yunlongn.mall.spring.core.aop;

import cn.yunlongn.mall.common.core.util.ToolUtil;
import cn.yunlongn.mall.spring.core.annotion.ConditionalOnPropertyMatchOrNot;
import org.springframework.boot.autoconfigure.condition.ConditionMessage;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Arrays;
import java.util.List;

/**
 * @author Yun
 */
@Order(Ordered.HIGHEST_PRECEDENCE + 50)
public class OnPropertyMatchOrNot extends SpringBootCondition {
    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context,
                                            AnnotatedTypeMetadata metadata) {
        String property1 = (String) metadata
                .getAnnotationAttributes(ConditionalOnPropertyMatchOrNot.class.getName())
                .get("property1");
        String property2 = (String) metadata
                .getAnnotationAttributes(ConditionalOnPropertyMatchOrNot.class.getName())
                .get("property2");
        String value = (String) metadata
                .getAnnotationAttributes(ConditionalOnPropertyMatchOrNot.class.getName())
                .get("value");
        String equ = (String) metadata
                .getAnnotationAttributes(ConditionalOnPropertyMatchOrNot.class.getName())
                .get("equ");
        String prop1Value = context.getEnvironment().resolvePlaceholders(property1);
        String prop2Value = context.getEnvironment().resolvePlaceholders(property2);
        boolean result = false;
        if ("Y".equalsIgnoreCase(equ)) {
            // 相等的话就创建Bean
            result = prop1Value.equalsIgnoreCase(prop2Value);
        }
        else {
            result = !prop1Value.equalsIgnoreCase(prop2Value);
        }
        return new ConditionOutcome(result, ConditionMessage
                .forCondition(ConditionalOnPropertyMatchOrNot.class, "(" + prop1Value +":" +prop2Value + ")")
                .resultedIn(result));
    }
}
