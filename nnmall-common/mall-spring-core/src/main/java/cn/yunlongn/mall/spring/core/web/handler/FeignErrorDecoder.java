package cn.yunlongn.mall.spring.core.web.handler;

import cn.yunlongn.mall.common.core.exception.ServiceException;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * 定义feign的异常处理类 在feign出异常的时候进入此类
 * 例如feign的密码错误
 * @author Yun
 */
@Configuration
public class FeignErrorDecoder implements ErrorDecoder {

    @Override

    public Exception decode(String methodKey, Response response) {
        try {
            // 这里直接拿到我们抛出的异常信息
            String message = Util.toString(response.body().asReader());
            try {
                JSONObject jsonObject = JSONObject.parseObject(message);
                return new ServiceException(jsonObject.getInteger("code"),jsonObject.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException ignored) {

        }
        return decode(methodKey, response);
    }
}