package cn.yunlongn.mall.spring.core.web.handler;

import cn.yunlongn.mall.common.core.util.MallUtil;
import cn.yunlongn.mall.common.core.vo.CommonResult;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class GlobalResponseBodyHandler implements ResponseBodyAdvice {


    /**
     * 判断什么时候应该 执行beforeBodyWrite方法
     * 这里每次请求都会进入 当这里返回true的时候 则调用下面的beforeBodyWrite方法
     * **/
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        if (returnType.getMethod() == null) {
            return false;
        }
        // 判断返回类型未CommonResult的时候就执行下面的方法
        return returnType.getMethod().getReturnType() == CommonResult.class;
    }

    /****
     * 返回值之前拦截 将放回的值 放在request 让后面日志可以打印返回的值
     * @param body CommonResult
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        MallUtil.setCommonResult(((ServletServerHttpRequest) request).getServletRequest(), (CommonResult) body);
        return body;
    }

}
